var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var vzdevkiID = [];
var geslaKanalov = [{kanal: "Skedenj", geslo: null}];

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    //console.log(vzdevkiGledeNaSocket);
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    preveriVeljavnostZasebnegaSporocila(socket,vzdevkiGledeNaSocket)
    obdelajPridruzitevKanalu(socket);
    posodobiUporabnike(socket, 'Skedenj')
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('uporabnikiServer', function(kanal) {
      posodobiUporabnike(socket, kanal);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  vzdevkiID.push({vzdevek: vzdevek, id: socket.id});
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.emit('obdelajKanal', {kanal: kanal, vzdevek: vzdevkiGledeNaSocket[socket.id]});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });
    var uporabnikiNaKanalu = io.sockets.clients(kanal);
    if (uporabnikiNaKanalu.length > 1) {
      var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        if (uporabnikSocketId != socket.id) {
          if (i > 0) {
            uporabnikiNaKanaluPovzetek += ', ';
          }
          uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
        }
      }
      uporabnikiNaKanaluPovzetek += '.';
      socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
    }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') === 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        vzdevkiID.push({vzdevek: vzdevek, id: socket.id});
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
        socket.emit('obdelajKanal', {kanal: trenutniKanal[socket.id], vzdevek: vzdevkiGledeNaSocket[socket.id]});
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function preveriVeljavnostZasebnegaSporocila(socket, vzdevkiGledeNaSocket) {
  var obstajaUporabnik = false;
  var IDprejemnika, uporabnikSocketId;
  socket.on('posljiZasebnoSporocilo', function(sporocilo){
    var vsiUporabniki = io.sockets.clients();
    
    
    for (var i in vzdevkiID) {
      if (sporocilo.prejemnik == vzdevkiID[i].vzdevek) {
        uporabnikSocketId = vzdevkiID[i].id;
        console.log(vzdevkiID[i].id + "vzdevek: " + vzdevkiID[i].vzdevek);
        uporabnikSocketId = vzdevkiID[i].id;
        obstajaUporabnik = true;
      }
    }
    
    /*for (var i in vzdevkiGledeNaSocket) {
      if (sporocilo.prejemnik == vzdevkiGledeNaSocket[i]) {
        IDprejemnika = vzdevkiGledeNaSocket[i];
        console.log("vzdevki: " + vzdevkiGledeNaSocket);
        for (var j in vsiUporabniki) {
          if(IDprejemnika == vsiUporabniki[j].id) {
            uporabnikSocketId = vsiUporabniki[j].id;
            console.log("Obstaja" + uporabnikSocketId);
            obstajaUporabnik = true;
          }  
        }
      }
    }*/
    if (obstajaUporabnik) {
      if (vzdevkiGledeNaSocket[socket.id] != sporocilo.prejemnik) {
        io.sockets.socket(uporabnikSocketId).emit('sporocilo', {besedilo: vzdevkiGledeNaSocket[socket.id] + " (zasebno): "+ sporocilo.sporocilo});
        socket.emit('sporocilo', {besedilo: '(zasebno za ' + sporocilo.prejemnik + '): ' + sporocilo.sporocilo});
        obstajaUporabnik = false;
      }
      else {
        io.sockets.socket(socket.id).emit('sporocilo', {besedilo: 'Sporočila  "'+ sporocilo.sporocilo + '" uporabniku z vzdevkom "' + sporocilo.prejemnik + '" ni bilo mogoče posredovati.'});
        obstajaUporabnik = false;
      }
    }
    else {
        io.sockets.socket(socket.id).emit('sporocilo', {besedilo: 'Sporočila  "'+ sporocilo.sporocilo + '" uporabniku z vzdevkom "' + sporocilo.prejemnik + '" ni bilo mogoče posredovati.'});
        obstajaUporabnik = false;
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    var obstajaKanal = false;
    
    for (var i in geslaKanalov) {
      if (kanal.novKanal == geslaKanalov[i].kanal && kanal.geslo == geslaKanalov[i].geslo) {
        //kanal in geslo sta pravilna
        obstajaKanal = true;
        socket.leave(trenutniKanal[socket.id]);
        pridruzitevKanalu(socket, kanal.novKanal);
        break;
      } else if (kanal.novKanal == geslaKanalov[i].kanal && kanal.geslo != geslaKanalov[i].geslo) {
        //kanal obstaja, geslo je napacno
        obstajaKanal = true;
        console.log("V stavku z napacnim geslom");
        if (geslaKanalov[i].geslo != null) {
          socket.emit('sporocilo', {besedilo: 'Pridružitev v kanal "' + kanal.novKanal + '" ni bilo uspešno, ker je geslo napačno!'});
        } else {
          socket.emit('sporocilo', {besedilo: 'Izbrani kanal ' + kanal.novKanal + ' je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom.'});
        }
        break;
      }
    }
    
    if (obstajaKanal !== true) {
        //kanal ne obstaja, zato ga kreiraj in po potrebi dodeli geslo
        socket.leave(trenutniKanal[socket.id]);
        pridruzitevKanalu(socket, kanal.novKanal);
        geslaKanalov.push({kanal: kanal.novKanal, geslo: kanal.geslo});  
        console.log("dodajam nov kanal");
        posodobiUporabnike(socket, kanal.novKanal);
        socket.emit('sporocilo', {besedilo: 'Uspešno ste kreirali nov kanal!'});
    }
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}

function posodobiUporabnike (socket, kanal) {
  var uporabniki = [];
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  
  for (var i in uporabnikiNaKanalu) {
    var uporabnikSocketId = uporabnikiNaKanalu[i].id;
    uporabniki.push(vzdevkiGledeNaSocket[uporabnikSocketId]);
  }
  socket.emit('uporabniki', uporabniki);
  socket.broadcast.to(kanal).emit('uporabniki', uporabniki);
}